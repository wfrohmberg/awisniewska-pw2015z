<?php
include __DIR__ . '/../classes/DiGraphImpl.php';

if (isset ( $_GET ['generate'] )) {
	$ctrl = new GraphController ();
	echo json_encode ( array (
			'status' => 'ok',
			'graph' => $ctrl->getGraph () 
	) );
} elseif (isset ( $_GET ['xmlExport'] ) && isset ( $_POST ['graph'] )) {
	$ctrl = new GraphController ();
	$ctrl->xmlExport ( $_POST ['graph'] );
} elseif (isset ( $_GET ['fileUpload'] ) && isset ( $_FILES )) {
	$ctrl = new GraphController ();
	$ctrl->grafUpload ();
} else {
	echo json_encode ( array (
			'status' => 'blad!' 
	) );
}
class GraphController {
	public function getGraph() {
		$randomGraph = new DiGraph ();
		return $randomGraph->generateGraph ( 10 );
	}
	public function xmlExport($graphData) {
		$gdEncoded = json_decode ( $graphData );
		$graphXml = new SimpleXMLElement ( '<graph/>' );
		foreach ( $gdEncoded as $v => $node ) {
			$nodeElement = $graphXml->addChild ( 'node' );
			$nodeElement->addChild ( 'index', $node->index );
			$coords = $nodeElement->addChild ( 'coords' );
			$coords->addChild ( 'x', $node->coords->x );
			$coords->addChild ( 'y', $node->coords->y );
			$adjacent = $nodeElement->addChild ( 'adjacent' );
			foreach ( $node->adjacent as $adj ) {
				$adjacent->addChild ( 'adj', $adj );
			}
		}
		
		$file = 'graphSerialized.xml';
		if (! file_put_contents ( $file, $graphXml->asXML () )) {
			echo json_encode ( array (
					'status' => 'blad' 
			) );
		} else {
			if (file_exists ( $file )) {
				echo json_encode ( array (
						'status' => 'ok' 
				) );
			} else {
				echo json_encode ( array (
						'status' => 'blad',
						'msg' => 'Nie ma takiego pliku!' 
				) );
			}
		}
	}
	public function grafUpload() {
		
		try {
			
			$target_dir = "../uploads/";
			$target_file = $target_dir . basename ( $_FILES ["fileToUpload"] ["name"] );
				$xml = XMLReader::open ( $_FILES ["fileToUpload"] ["tmp_name"] );
				$xml->setParserProperty ( XMLReader::VALIDATE, true );
				if (! $xml->isValid ()) {
					echo json_encode ( array (
							'status' => 'blad',
							'msg' => 'Xml ma niepoprawny format!' 
					));
				} else {
					if (move_uploaded_file ( $_FILES ["fileToUpload"] ["tmp_name"], $target_file )) {
						$uploadedGraphArray = $this->xmlToArray($target_file);
// 						print_r($uploadedGraphArray['node']);exit;
						$nodes = array();
						$edges = array();
						foreach ($uploadedGraphArray['node'] as $i => $n){
							$node = new Node($n['coords']['x'], $n['coords']['y']);
							$node->setIndex($i);
							$edges[$i] = array();
							foreach ($n['adjacent']['adj'] as $adj){
								$edges[$i][] = $adj;
							}							
							$nodes[$i] = $node;
						}
						
						echo json_encode ( array (
								'status' => 'ok',
								'graph' => array(
										'nodes' => $nodes,
										'edges' => $edges
								)	 
						) );
						exit;
					} else {
						echo json_encode ( array (
								'status' => 'blad',
								'msg' => 'Wystąpił problem podczas zapisu pliku' 
						) );
					}
				}
			
		} catch ( \Exception $e ) {
			echo json_encode ( array (
					'status' => 'blad',
					'msg' => 'Wystapił jakiś błąd!' 
			) );
		}
	}
	public function xmlToArray($target_file){
		$xmltoarray = simplexml_load_file($target_file);
		return unserialize(serialize(json_decode(json_encode((array) $xmltoarray), 1)));
	}
}


