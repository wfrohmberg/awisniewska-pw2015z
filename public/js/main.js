(function(){
	var diGraph = {
		properties: {
			graphWidth:700,
			graphHeight: 500,
			circleRadius: 200,
			graphControllerPath:'../controllers/GraphController.php',
		},
		nodes: [],
		edges: [],
		cells: [],
		clearGraph: function(graph){
			graph.clear();
			this.nodes = [];
			this.edges = [];
			this.cells = [];
		},
		preparegraph: function(data){
			for(var i = 0; i<data.graph.nodes.length; i++){
				  var rect = new joint.shapes.basic.Rect({
					    position: { x: data.graph.nodes[i].x, y: data.graph.nodes[i].y},
					    size: { width: 30, height: 30 },
					    attrs: { rect: { fill: 'blue' }, text: { text: data.graph.nodes[i].index, fill: 'white' } }
					});	
				  diGraph.nodes.push(rect);
			  }
			  for(var j = 0; j<data.graph.edges.length; j++){
				  for(var k = 0; k<data.graph.edges[j].length; k++){
					  var link = new joint.dia.Link({
							source: { id: diGraph.nodes[j].id },
							target: { id: diGraph.nodes[data.graph.edges[j][k]] }
						});
					  link.attr({
						    '.connection': { stroke: 'blue' },
						    '.marker-target': { fill: 'yellow', d: 'M 10 0 L 0 5 L 10 10 z' }
						});
					  var cell = [diGraph.nodes[j], diGraph.nodes[data.graph.edges[j][k]], link];
					  diGraph.cells.push(cell);
				  }
			  }
		}
	};
	var xmlReady = false;
	$('document').ready(function(){
		graph = new joint.dia.Graph,
		paper = new joint.dia.Paper({
		    el: $('#graph'),
		    width: diGraph.properties.graphWidth,
		    height: diGraph.properties.graphHeight,
		    model: graph,
		    gridSize: 1
		}),
		$('#add').hide();
		$('#circle').hide();
		$('#export-to-xml').hide();
		
		$('#generate').click(function(){
			diGraph.clearGraph(graph);
			$.ajax({
				  timeout: 10000,
				  method: "GET",
				  url: diGraph.properties.graphControllerPath + "?generate=1",
				  dataType: "json"
				})
				  .error(function(){alert("Nie udało się utworzyć grafu!")})
				  .done(function( msg ) {
					  diGraph.preparegraph(msg);
					  graph.addCells(diGraph.cells);
					  $('#add').show();
					  $('#circle').show();
					  $('#export-to-xml').show();
				  });
		});
		$('#add').click(function(){
			var rect = new joint.shapes.basic.Rect({
			    position: { x: Math.floor((Math.random() * 600) + 1), y: Math.floor((Math.random() * 300) + 1)},
			    size: { width: 30, height: 30 },
			    attrs: { rect: { fill: 'blue' }, text: { text: diGraph.nodes.length, fill: 'white' } }
			});
			lastLink = new joint.dia.Link({
				source: { id: rect.id },
				target: { id: diGraph.nodes[diGraph.nodes.length-1]}
			});
			lastLink.attr({
			    '.connection': { stroke: 'blue' },
			    '.marker-target': { fill: 'yellow', d: 'M 10 0 L 0 5 L 10 10 z' }
			});
			
			diGraph.nodes.push(rect);
			graph.addCells([rect,diGraph.nodes[diGraph.nodes.length-1], lastLink]);
		});
		$('#circle').click(function(){
			var width = diGraph.properties.graphWidth,
			    height = diGraph.properties.graphHeight,
			    angle = 0,
			    step = (2*Math.PI) / diGraph.nodes.length;
			for(var j = 0; j<diGraph.nodes.length; j++){
				var xx = Math.round(width/2 + diGraph.properties.circleRadius * Math.cos(angle)),
		        yy = Math.round(height/2 + diGraph.properties.circleRadius * Math.sin(angle));
				diGraph.nodes[j].set({'position':{x:xx, y:yy}});
			    angle += step;
			    //console.log(diGraph.nodes[j].get('position'));
			}
		});
		$('#export-to-xml').click(function(){
			var nodesShort = [];
			var elements = graph.getElements();
			
			for(var j = 0; j<elements.length; j++){//dla każdego wierzchłka
				nodesShort.push({
					'index': elements[j].attributes.attrs.text.text,
					'coords': elements[j].get('position'),
					'adjacent': []
				});			
				var neighbours = graph.getNeighbors(elements[j], {'opt': {outbound: true, inbound:false, deep:false}});
				for(var k = 0; k<neighbours.length; k++){
					nodesShort[j].adjacent.push(neighbours[k].attributes.attrs.text.text);
				}
				//console.log(nodesShort[elements[j].id]['adjacent']);
			}		
			var str = JSON.stringify(nodesShort);
			$.ajax({
				  method: "POST",
				  url: diGraph.properties.graphControllerPath + "?xmlExport=1",
				  dataType: 'xml',
				  data: { 'graph': str }
				})
				  .error(function(){
					  alert('Nie udało się wyeksportować grafu!')
				  })
				  .done(function( msg ) {
					  var win= window.open("http://localhost/graph_viewer/awisniewska-pw2015z/controllers/graphSerialized.xml", '_blank');					  
					  win.focus();
				  });
		});
		
		$(':file').change(function(){
		    var file = this.files[0];
		    var name = file.name;
		    var size = file.size;
		    var type = file.type;
		});
		$('#upload-grafu').click(function(e){
			e.preventDefault();
			diGraph.clearGraph(graph);
		    var formData = new FormData($('form')[0]);
		    $.ajax({
		        url: '../controllers/GraphController.php?fileUpload=1', 
		        type: 'POST',
		        xhr: function() { 
		            var myXhr = $.ajaxSettings.xhr();
		            return myXhr;
		        },
		        beforeSend: function(){},
		        success: function(msg){
		        	var data = JSON.parse(msg);
		        	if(data.status === 'ok'){
		        		diGraph.preparegraph(data);
						  graph.addCells(diGraph.cells);
						  $('#add').show();
						  $('#circle').show();
						  $('#export-to-xml').show();
		        	}
		        	
		        },
		        error: function(){alert('blad')},
		        data: formData,
		        cache: false,
		        contentType: false,
		        processData: false
		    })
		});
		paper.on('cell:pointerdown', 
			    function(element, evt, x, y) { 
					element.model.attr({
					    rect: { fill: 'black' },
					    text: { fill: 'white', 'font-size': 15 },
					    '.myrect2': { fill: 'red' }
					});  
					$('html').keyup(function(e){
					    if(e.keyCode == 46){
					    	element.model.remove({'options':{'disconnectLinks':true}});
					    }
					}) 
			    }
			);
	});
	
})();
