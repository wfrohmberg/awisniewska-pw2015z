<?php
include 'NodeImpl.php';
class DiGraph{
	private  $nodes = array();
	private  $edges = array();
	
	public function addNode(){
		
	}
	public function removeNode(){
		
	}
	public function addEdge(){
		
	}
	public function removeEdge(){
		
	}
	public function getNodes(){
		return $this->nodes;
	}
	public function getEdges(){
		return $this->edges;
	}
	
	public function generateGraph($nodesCount){
		for($i = 0; $i < $nodesCount; $i++){
			$node = new Node(rand(0, 600), rand(0,300));
			$node->setIndex($i);
			$howManyEdges = rand(0, 5);
			$this->edges[$i] = array();
			for ($j = 0; $j<$howManyEdges; $j++ ){
				$newEdge = $this->newEdge($this->edges[$i], $nodesCount-1, $i);
				if($newEdge === null || $newEdge === false){
					continue;
				}
				$this->edges[$i][] = $newEdge;
			}
			
			$this->nodes[$i] = $node;
		}
		
		return array('nodes'=>$this->getNodes(), 'edges' => $this->getEdges());
	}
	
	public function newEdge($source, $nodes, $current) {
		$edge = rand(0, $nodes);
		if(in_array($edge, $source) OR ($edge == $current))
			return false;
		else
			return (int)$edge;
	}
}