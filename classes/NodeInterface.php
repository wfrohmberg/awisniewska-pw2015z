<?php

interface NodeInterface {
	public function __construct($x, $y);
	 public function setIndex($index);
	 public function getIndex();
	 public function setX($x);
	 public function getX();
	 public function setY($y);
	 public function getY();
	 public function setNodeName($name);
	 public function getNodeName();
}