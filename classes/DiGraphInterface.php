<?php

interface DiGraphInterface {
	private  $nodes = array();
	private  $edges = array();
	
	public function addNode();
	public function removeNode();
	public function addEdge();
	public function removeEdge();
}