<?php
include 'NodeInterface.php';
class Node implements NodeInterface{
	public  $index;
	public $x;
	public $y;
	private $name;
	public function __construct($x, $y){
		$this->x = $x;
		$this->y = $y;
	}
	public function setIndex($index){
		$this->index = $index;
	}
	public function getIndex(){
		return $this->index;
	}
	public function setX($x){
		$this->x = $x;
	}
	public function getX(){
		return $this->x;
	}
	public function setY($y){
		$this->y = $y;
	}
	public function getY(){
		return $this->y;
	}
	public function setNodeName($name){
		$this->name = $name;
	}
	public function getNodeName(){
		return $this->name;
	}
}